Source: pidcat
Section: devel
Priority: optional
Maintainer: Hans-Christoph Steiner <hans@eds.org>
Uploaders: Debian Python Team <team+python@tracker.debian.org>
Build-Depends: python3-all,
               debhelper-compat (= 13),
               bash-completion
Standards-Version: 4.1.3
Homepage: https://github.com/JakeWharton/pidcat
Vcs-Git: https://salsa.debian.org/python-team/packages/pidcat.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/pidcat
Rules-Requires-Root: no

Package: pidcat
Architecture: all
Depends: python3:any, ${misc:Depends}
Recommends: android-tools-adb
Description: Colored adb logcat that shows entries for specific apps
 An update to Jeff Sharkey's excellent logcat color script which only shows
 log entries for processes from a specific application package.
 .
 During application development you often want to only display log messages
 coming from your app. Unfortunately, because the process ID changes every
 time you deploy to the phone it becomes a challenge to grep for the right
 thing.
 .
 This script solves that problem by filtering by application package. Supply
 the target package as the sole argument to the Python script and enjoy a more
 convenient development process.
 .
  pidcat com.oprah.bees.android
